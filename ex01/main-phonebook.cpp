/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/22 20:25:56 by davifah           #+#    #+#             */
/*   Updated: 2022/06/23 09:16:15 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"

#include <cstdio>

using std::string;
using std::cout;
using std::cin;

int	main(void)
{
	PhoneBook pb;

	cout << "Welcome to the PhoneBook program!\n"
		"Available commands: ADD, SEARCH and EXIT\n";
	while (1)
	{
		string	prompt_input;

		cout << "Enter command> ";
		std::getline(std::cin, prompt_input);
		if (!prompt_input.compare("ADD"))
			pb.add();
		else if (!prompt_input.compare("SEARCH"))
		{
			int	search_res = 1;
			search_res = !pb.print_array_in_columns();
			while (search_res)
			{
				cout << "\tEnter Contact index> ";
				std::getline(std::cin, prompt_input);
				if (sscanf(prompt_input.c_str(), "%d", &search_res) == 1)
					search_res = pb.search(search_res);
				if (search_res)
					cout << "\tInvalid Index\n";
			}
		}
		else if (!prompt_input.compare("EXIT"))
		{
			cout << "Exiting Phonebook\n";
			break ;
		}
		else
			cout << "Command not found: '" << prompt_input << "'\n";
	}
	return (0);
}
