/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/22 21:05:14 by davifah           #+#    #+#             */
/*   Updated: 2022/06/27 23:20:07 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP

#include "Contact.hpp"

class PhoneBook
{
	private:
		const static int array_size = 8;
		Contact *c_array[array_size];

	public:
		void	add(void);
		int		search(int index);
		int		print_array_in_columns(void);

		PhoneBook();
		~PhoneBook();
};

#endif
