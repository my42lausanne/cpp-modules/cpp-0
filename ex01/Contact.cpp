/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/22 20:11:45 by davifah           #+#    #+#             */
/*   Updated: 2022/10/18 12:05:11 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"
#include <cstring>

Contact::Contact()
{
	time_of_creation = std::time(NULL);
	get_info();
}

void	Contact::print_info()
{
	std::cout << "First Name: " << first_name << std::endl;
	std::cout << "Last Name: " << last_name << std::endl;
	std::cout << "Nickname: " << nickname << std::endl;
	std::cout << "Phone Number: " << phone_number << std::endl;
	std::cout << "Darkest Secret: " << darkest_secret << std::endl;
}

void	Contact::get_info()
{
	std::cout << "Please enter all of the necessary information:\n";
	std::cout << "First Name: ";
	std::getline(std::cin, first_name);
	std::cout << "Last Name: ";
	std::getline(std::cin, last_name);
	std::cout << "Nickname: ";
	std::getline(std::cin, nickname);
	std::cout << "Phone Number: ";
	std::getline(std::cin, phone_number);
	std::cout << "Darkest Secret: ";
	std::getline(std::cin, darkest_secret);

	if (first_name.length() && last_name.length() && nickname.length()
			&& phone_number.length() && darkest_secret.length())
		return ;
	std::cout << "All fields must contain information!\n";
	get_info();
}

static void	truncate_dot(char *dest, std::string origin)
{
	std::memset(dest, 0, 11);
	if (origin.length() > 10)
	{
		origin.copy(dest, 10);
		dest[9] = '.';
	}
	else
		origin.copy(dest, origin.length());
}

void	Contact::print_columns(int index)
{
	char h_f_name[11], h_l_name[11], h_n_name[11];

	truncate_dot(h_f_name, first_name);
	truncate_dot(h_l_name, last_name);
	truncate_dot(h_n_name, nickname);

	std::cout << std::setw(10) << std::right << index;
	std::cout << "|" << std::setw(10) << std::right << h_f_name;
	std::cout << "|" << std::setw(10) << std::right << h_l_name;
	std::cout << "|" << std::setw(10) << std::right << h_n_name;
	std::cout << "\n";
}

std::time_t	Contact::get_time_of_creation(void)
{
	return (time_of_creation);
}
