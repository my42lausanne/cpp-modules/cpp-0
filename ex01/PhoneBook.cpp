/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/22 21:10:58 by davifah           #+#    #+#             */
/*   Updated: 2022/10/18 12:04:33 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"

PhoneBook::PhoneBook()
{
	for (int i = 0; i < array_size; i++)
		c_array[i] = 0;
}

PhoneBook::~PhoneBook()
{
	for (int i = 0; i < array_size; i++)
	{
		if (c_array[i])
			delete c_array[i];
	}
}

void	PhoneBook::add()
{
	Contact *c_new;

	c_new = new Contact;

	int	oldest;
	for (int i = 0; i < array_size; i++)
	{
		if (!c_array[i])
		{
			c_array[i] = c_new;
			return ;
		}
		if (!i ||
			c_array[oldest]->get_time_of_creation() > c_array[i]->get_time_of_creation())
			oldest = i;
	}
	delete c_array[oldest];
	c_array[oldest] = c_new;
}

int	PhoneBook::print_array_in_columns(void)
{
	if (!c_array[0])
	{
		std::cout << "\tThe PhoneBook is empty\n";
		return (1);
	}
	for (int i = 0; i < array_size && c_array[i]; i++)
		c_array[i]->print_columns(i);
	return (0);
}

int	PhoneBook::search(int index)
{
	if (index < 0 || index >= array_size || !c_array[index])
		return (1);
	c_array[index]->print_info();
	return (0);
}
