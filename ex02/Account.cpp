/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Account.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/23 12:21:54 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/23 16:33:48 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Account.hpp"
#include <iostream>
#include <ctime>
#include <iomanip>

#define LOG 1

using std::cout;

int	Account::_nbAccounts = 0;
int	Account::_totalAmount = 0;
int	Account::_totalNbDeposits = 0;
int	Account::_totalNbWithdrawals = 0;

Account::Account(int initial_deposit)
{
	_accountIndex = _nbAccounts;
	_amount = initial_deposit;
	_nbDeposits = 0;
	_nbWithdrawals = 0;

	_nbAccounts++;
	_totalAmount += initial_deposit;

	if (LOG)
	{
		_displayTimestamp();
		cout << " index:" << _accountIndex << ";amount:" << _amount
			<< ";created\n";
	}
}

Account::~Account()
{
	if (LOG)
	{
		_displayTimestamp();
		cout << " index:" << _accountIndex << ";amount:" << _amount << ";closed\n";
	}
}

void	Account::makeDeposit(int deposit)
{
	const int	p_amount = _amount;

	_nbDeposits++;
	_amount += deposit;

	_totalNbDeposits++;
	_totalAmount += deposit;

	if (LOG)
	{
		_displayTimestamp();
		cout << " index:" << _accountIndex << ";p_amount:" << p_amount
			<< ";deposit:" << deposit << ";amount:" << _amount << ";nb_deposits:"
			<< _nbDeposits << std::endl;
	}
}

bool	Account::makeWithdrawal(int withdrawal)
{
	const int	p_amount = _amount;
	const bool	refuse = withdrawal > _amount;

	if (!refuse)
	{
		_nbWithdrawals++;
		_amount -= withdrawal;

		_totalNbWithdrawals++;
		_totalAmount -= withdrawal;
	}
	if (LOG)
	{
		_displayTimestamp();
		cout << " index:" << _accountIndex << ";p_amount:" << p_amount
			<< ";withdrawal:";
		if (refuse)
			cout << "refused" << std::endl;
		else
			cout << withdrawal << ";amount:" << _amount << ";nb_withdrawals:"
				<< _nbWithdrawals << std::endl;
	}
	return (refuse);
}

int	Account::checkAmount( void ) const
{
	return (_amount);
}

void	Account::displayStatus( void ) const
{
	_displayTimestamp();
	cout << " index:" << _accountIndex << ";amount:" << _amount << ";deposits:"
		<< _nbDeposits << ";withdrawals:" << _nbWithdrawals << std::endl;
}

void	Account::_displayTimestamp( void )
{
	struct tm	*time;
	std::time_t	t = std::time(NULL);

	time = localtime(&t);
	cout << "[" << 1900 + time->tm_year;
	cout << std::setfill('0') << std::setw(2) << 1 + time->tm_mon;
	cout << std::setfill('0') << std::setw(2) << time->tm_mday << "_";
	cout << std::setfill('0') << std::setw(2) << time->tm_hour;
	cout << std::setfill('0') << std::setw(2) << time->tm_min;
	cout << std::setfill('0') << std::setw(2) << time->tm_sec << "]";
}

int	Account::getNbAccounts()
{
	return (_nbAccounts);
}

int Account::getTotalAmount()
{
	return (_totalAmount);
}

int	Account::getNbDeposits()
{
	return (_totalNbDeposits);
}

int	Account::getNbWithdrawals()
{
	return (_totalNbWithdrawals);
}

void	Account::displayAccountsInfos()
{
	_displayTimestamp();
	cout << " accounts:" << getNbAccounts() << ";total:" << getTotalAmount()
		<< ";deposits:" << getNbDeposits() << ";withdrawals:" << getNbWithdrawals()
		<< "\n";
}
